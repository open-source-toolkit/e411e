# Qt使用QtWebApp搭建Http服务器实现文件下载

## 简介

本项目演示了如何使用Qt和QtWebApp库搭建一个简单的HTTP服务器，并实现文件下载功能。通过本项目，您可以学习如何在Qt环境中集成QtWebApp库，创建一个基本的HTTP服务器，并处理文件下载请求。

## 功能特点

- **HTTP服务器搭建**：使用QtWebApp库快速搭建一个HTTP服务器。
- **文件下载**：实现文件下载功能，客户端可以通过浏览器或HTTP客户端工具下载服务器上的文件。
- **简单易用**：代码结构清晰，易于理解和扩展。

## 环境要求

- Qt 5.x 或更高版本
- QtWebApp库

## 安装与配置

1. **克隆仓库**：
   ```bash
   git clone https://github.com/yourusername/your-repo.git
   ```

2. **安装QtWebApp库**：
   您可以从[QtWebApp官方仓库](https://github.com/qtwebapp/qtwebapp)下载并安装QtWebApp库。

3. **编译项目**：
   使用Qt Creator打开项目文件（.pro），配置好QtWebApp库的路径后，编译并运行项目。

## 使用说明

1. **启动服务器**：
   运行编译后的可执行文件，服务器将默认在本地8080端口启动。

2. **访问服务器**：
   打开浏览器，访问`http://localhost:8080/download?file=yourfile.txt`，其中`yourfile.txt`是服务器上可供下载的文件名。

3. **文件下载**：
   服务器将根据请求的文件名，将文件发送给客户端进行下载。

## 示例代码

以下是一个简单的示例代码，展示了如何使用QtWebApp库处理文件下载请求：

```cpp
#include "httprequesthandler.h"
#include "httpresponse.h"
#include "httprequest.h"

class FileDownloadHandler : public HttpRequestHandler {
public:
    void service(HttpRequest& request, HttpResponse& response) override {
        QString fileName = request.getParameter("file");
        QFile file("path/to/your/files/" + fileName);
        if (file.open(QIODevice::ReadOnly)) {
            response.setHeader("Content-Type", "application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            response.write(file.readAll());
            file.close();
        } else {
            response.setStatus(404, "File Not Found");
            response.write("File not found", true);
        }
    }
};
```

## 贡献

欢迎提交Issue和Pull Request，共同完善本项目。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

---

通过本项目，您可以快速上手使用Qt和QtWebApp搭建HTTP服务器，并实现文件下载功能。希望本项目对您的学习和开发有所帮助！